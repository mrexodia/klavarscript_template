#include <windows.h>
#include <commctrl.h>
#include <stdio.h>
#include "resource.h"
#include "shlobj.h"

HINSTANCE hInst;

BOOL CALLBACK DlgMain(HWND hwndDlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    switch(uMsg)
    {
    case WM_INITDIALOG:
    {
        SendMessageA(hwndDlg, WM_SETICON, ICON_BIG, (LPARAM)LoadIconA(hInst, MAKEINTRESOURCE(IDI_ICON1)));
        SetDlgItemTextA(hwndDlg, IDC_STC_LOG, "Ready to install...");
    }
    return TRUE;

    case WM_CLOSE:
    {
        EndDialog(hwndDlg, 0);
    }
    return TRUE;

    case WM_COMMAND:
    {
        switch(LOWORD(wParam))
        {
        case IDC_BTN_INSTALL:
        {
            //CSIDL_MYDOCUMENTS
            char file[MAX_PATH]="";
            SetFocus(GetDlgItem(hwndDlg, IDC_STC_LOG));
            SHGetFolderPathA(0, CSIDL_PERSONAL, 0, 0, file);

            strcat(file, "\\KlavarScript\\Templates\\2ROW.kst");
            DeleteFileA(file);
            HANDLE hFile=CreateFileA(file, GENERIC_READ|GENERIC_WRITE, FILE_SHARE_READ, 0, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
            if(hFile==INVALID_HANDLE_VALUE)
            {
                SetDlgItemTextA(hwndDlg, IDC_STC_LOG, "Failed to create template file...");
                return TRUE;
            }
            HRSRC res=FindResource(NULL, "TWOROW", "BINARY");
            if(!res)
            {
                CloseHandle(hFile);
                SetDlgItemTextA(hwndDlg, IDC_STC_LOG, "Failed to find resource...");
                return TRUE;
            }
            DWORD written=0;
            if(!WriteFile(hFile, LoadResource(NULL, res), SizeofResource(0, res), &written, NULL))
            {
                CloseHandle(hFile);
                SetDlgItemTextA(hwndDlg, IDC_STC_LOG, "Failed to write file...");
                return TRUE;
            }
            CloseHandle(hFile);
            SetDlgItemTextA(hwndDlg, IDC_STC_LOG, "Template installed!");
            EnableWindow(GetDlgItem(hwndDlg, IDC_BTN_INSTALL), 0);
        }
        return TRUE;
        }
    }
    return TRUE;
    }
    return FALSE;
}


int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd)
{
    hInst=hInstance;
    InitCommonControls();
    return DialogBox(hInst, MAKEINTRESOURCE(DLG_MAIN), NULL, (DLGPROC)DlgMain);
}
